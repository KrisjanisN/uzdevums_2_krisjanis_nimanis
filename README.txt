Labdien, šeit Krišjānis Nīmanis.
Uzdevums nebija viegls. Pa karantīnu un vasaru biju ierūsējis.
Atvainojos par spamu e-pastā..

Teikšu godīgi, nebiju domājis, ka mācīšos par programmētāju. Pēc 9. klases vajadzēja kaut kur iet,
atzīmes bija labas, mācīties es mācījos, bija jādomā kur iet. Iztaigāju visādas izstādes un 
izskatīju iespējas un nolēmu aiziet uz Rīgas Tehnisko koledžu skatīties kādas būs manas attiecības ar 
programmēšanu. Neko jau nevar zaudēt
Neesmu ne dienu nožēlojis, protams mēdz šķist, ka esmu apmaldījies,
bet tas viss ietilpst augšanas procesā.
Personīgi, vēlētos pašlaikt izveidot pilna apjoma mājas lapu. Ar aizraujošu front-end (pats izdomāt 
dizainu) un ne tik vienkāršu back-end.
Uzskatu, ka front-end tomēr ir man stiprāka un interesntāka puse.

Veidojot uzdevumu, pielietoju bootstrap un Scss. 
Rakstīju es Scss formātā un speciāls extension visu pārveidoja uz CSS, tāpēc domāju, ka nekas īpašs nav jādara, lai redzētu kodu.

Gaidīšu ziņu no Jums,
ļoti novērtētu, ja sanāktu pēc iespējas ātrāk. 
Pat,ja sliktas ziņas, tāpat būtu labāk viņas uzzināt ātrāk nekā vēlāk.

Liels paldies par iespēju, cerams uz drīz tikšanos.

Pieliku klāt vēl bildi ar laika izpildi
----------------------------------
Repesatorija
git clone https://KrisjanisN@bitbucket.org/KrisjanisN/uzdevums_2_krisjanis_nimanis.git
-----------------------------------
GitPage
https://kukais-chirka.github.io./
